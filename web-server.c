#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	struct sockaddr_in c_addr;
	char fname[100];

	int err;
	struct sockaddr_in serv_addr;
	int listenfd = 0,ret;
	char sendBuff[1025];
	int numrv;

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if(listenfd<0)
	{
		printf("Error in socket creation\n");
		exit(2);
	}
	char ch[]="127.0.0.1";
	char port[]="4000";
	
	if (argv[2] != NULL)
	strcpy(port,argv[2]); 
	if(argv[1]!=NULL)
	strcpy(ch,argv[1]);// = "127.0.0.1";

	

	//printf(" IP Address %s",ch);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	serv_addr.sin_port = htons(4000);

	//printf("\nBefore server binding, argv[%d] is %s, argv[%d] is %s and argv[%d] is %s\n\n",1,argv[1],2,argv[2],3,argv[2] );
	//printf("%d     %d    %d",serv_addr.sin_family,serv_addr.sin_addr.s_addr,serv_addr.sin_port);
	//printf(" IP Address %d\n", inet_addr(ch));

	if(bind(listenfd, (struct sockaddr*)&serv_addr,sizeof(serv_addr))<0)
	{
		printf("Error in bind\n");
		exit(2);
	}

	if(listen(listenfd, 10) == -1)
	{
		printf("Failed to listen\n");
		return -1;
	}



	printf("\nServer ready to run\n");
	while(1)
	{
		size_t clen=0;

		clen=sizeof(c_addr);
		printf("Waiting for connection\n");
		int connfd = accept(listenfd, (struct sockaddr*)&c_addr, &clen);

		if(connfd<0)
		{
			printf("Error in accept\n");
			continue;	
		}

		if (fork()>0)
		{close (connfd) ;}
		else{
			char details[30];
			inet_ntop (c_addr.sin_family , &c_addr.sin_addr , details , sizeof(details) );
			printf("\nClient details - %s:%d", details, ntohs(c_addr.sin_port));
		}
			char Buffer[256];
			char send_message[256];
			if(recv(connfd , Buffer ,sizeof(Buffer) , 0) < 0 )
				printf("\nerror recieving\n");

			//BUffer holds the client request
			if ( Buffer[0]!= 'G' || Buffer[1]!='E' || Buffer[2] != 'T'  )
			{
				bzero(send_message,sizeof(send_message));				
				strcpy(send_message ,"HTTP/1.0 400 BAD REQUEST\r\n\r\n") ;
				printf("\nbad get request error\n");
				send (connfd , send_message , sizeof(send_message),0);
				exit(1);
			}

			else if(Buffer[0]== 'G' && Buffer[1]=='E' && Buffer[2]== 'T' )
			{
				char object_path[256];
				bzero (object_path , 256);
				int i;		
				for (i=0 ; i<4000 ; i++)
				{
					object_path[i] = Buffer[i+5];
					if (Buffer[i+6] == ' ')
						break;}
				printf("\n%s objct path\n",object_path); //until here no error BUT SEGMENTATION FAULT

				if( Buffer[i+7] != 'H' || Buffer[i+8] != 'T' || Buffer[i+9] != 'T' || Buffer[i+10] != 'P' || Buffer[i+11] != '/'||Buffer[i+12] != '1')
				{
					bzero(send_message,sizeof(send_message));					
					strcpy(send_message,"HTTP/1.0 400 BAD REQUEST\r\n\r\n"); ;
					printf("\nHttp bad written error\n");
					send (connfd , send_message , sizeof(send_message),0);
					break;
				}

				else if (Buffer[i+13]!='.' || !(Buffer[i+14] == '1' ||Buffer[i+15] == '0'))
				{
					bzero(send_message,sizeof(send_message));
					strcpy(send_message , "HTTP/1.0 400 BAD REQUEST\r\n\r\n") ;
					printf("\n. or 1 or 0 error\n");
					send (connfd , send_message , sizeof(send_message),0);
					break;	
				}

				FILE *fpoint;

				fpoint = fopen(object_path , "r");

				if (fpoint <= 0)
				{
					printf("\nfile opening error, sending 404 to client");
					bzero(send_message,sizeof(send_message));
					strcpy(send_message, "HTTP/1.0 404 NOT FOUND\r\n\r\n") ;
					send (connfd , send_message , sizeof(send_message),0);
					break;
				}
				else
				{
					bzero(send_message,sizeof(send_message));
					strcpy(send_message,"HTTP/1.0 200 OK\r\n\r\n") ;
					send (connfd , send_message , sizeof(send_message),0);
				}

				//sending file
				bzero(send_message,sizeof(send_message));
				while(1)
				{
					int recv_byte = fread (send_message , sizeof(char) , sizeof(send_message) , fpoint);
					if (recv_byte <= 0) break;
					send(connfd , send_message ,recv_byte , 0);

				}
			}
		//close(connfd);   
		}
		
		return 0;
	}
